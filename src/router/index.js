import Vue from 'vue';
import VueRouter from 'vue-router';
import LoginForm from '@/components/LoginForm.vue';
import Home from '@/components/HomeSection.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
  },
  {
    path: '/login',
    name: 'login',
    component: LoginForm,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
