# Instrucciones de Instalación

Estas son las instrucciones para instalar y ejecutar este proyecto Vue.js.

## Requisitos Previos
- [Node.js](https://nodejs.org/) instalado en tu máquina.
 (Se recomienda utilizar Node.js v18.17.0)

## Pasos de Instalación

1. **Clona el Repositorio:**
   ```bash
   git clone https://gitlab.com/pao25/movieproject.git
2. **Navega al Directorio del Proyecto:**
    ```bash
    cd movieproject

1. **Instala las Dependencias:**
    ```bash
   npm install
1. **Ejecutar el Proyecto:**
    ```bash
   npm run serve